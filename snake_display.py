import pygame
import random
import time
from Snake import Snake
from FoodSpawn import FoodSpawn
 
def gameOver(window, score):
    font = pygame.font.SysFont('Candara', 30)
    score_text = font.render("Your Score " + str(score*5) ,4,(255,0,0))
    window.blit(score_text,(210,250))
    pygame.display.flip()
    time.sleep(1)
    pygame.quit()

def run_snake():
    pygame.init()
    window = pygame.display.set_mode((500 + 20,500 + 20))
    fps = pygame.time.Clock()
    score = 0
    snake = Snake()
    food_generat = FoodSpawn()
    crashed = False
    while not crashed:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameOver(window, score)
                crashed = True
            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_RIGHT]:
                snake.change_dir_to('RIGHT')
            elif pressed[pygame.K_LEFT]:
                snake.change_dir_to('LEFT')
            elif pressed[pygame.K_UP]:
                snake.change_dir_to('UP')
            elif pressed[pygame.K_DOWN]:
                snake.change_dir_to('DOWN')
            elif pressed[pygame.K_ESCAPE]:
                gameOver(window, score)
                crashed = True
    
        food_position = food_generat.spawnFood()
        if( snake.move( food_position) == 1 ):
            score += 1
            food_generat.setFoodOnScreen(False)
    
        window.fill(pygame.Color( 225, 225, 225 ))

        for x in range(0, 520, 10):
            pygame.draw.rect(window, (74, 77, 75), [510, x, 10, 10])
    
        for pos in snake.get_body():
            pygame.draw.rect(window, pygame.Color(0, 225, 0), pygame.Rect(pos[0], pos[1], 10, 10))
        pygame.draw.rect(window, pygame.Color(225, 0, 0), pygame.Rect(food_position[0], food_position[1], 10, 10 ) )
        
        if( snake.check_collision() == 1 ):
            gameOver( window, score)
            crashed = True
        
        pygame.display.set_caption("Snake | Score: " + str(score*5))
        pygame.display.flip()
        fps.tick(20)
    pygame.quit()

run_snake()