import random


class FoodSpawn():
    def __init__(self):
        self.position = [random.randint(4,46)*10,random.randint(4,46)*10]
        self.isFoodOnScreen = True
 
    def spawnFood(self):
        if self.isFoodOnScreen == False:
            self.position = [random.randrange(4,46)*10,random.randrange(4,46)*10]
            self.isFoodOnScreen = True
        return self.position
     
    def setFoodOnScreen(self,b):
        self.isFoodOnScreen = b